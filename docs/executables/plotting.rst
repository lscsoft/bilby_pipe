===============
bilby_pipe_plot
===============

Command line interface for plotting result
------------------------------------------

There are additional executables to generate specific plots

- :code:`bilby_pipe_plot_corner`
- :code:`bilby_pipe_plot_skymap`
- :code:`bilby_pipe_plot_calibration`
- :code:`bilby_pipe_plot_marginal`
- :code:`bilby_pipe_plot_waveform`

.. argparse::
   :module: bilby_pipe.plot
   :func: create_parser
   :prog: bilby_pipe_plot
