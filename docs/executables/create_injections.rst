============================
bilby_pipe_create_injections
============================

Command line interface for creating an injection set
----------------------------------------------------

.. argparse::
   :module: bilby_pipe.create_injections
   :func: create_parser
   :prog: bilby_pipe_create_injections
