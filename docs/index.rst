.. automodule:: bilby_pipe
   :no-index:

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   user-interface
   examples
   ini_file
   injections
   global_settings
   customisation
   data_acquisition
   reweighting
   osg
   asimov
   structure
   contributing

.. toctree::
   :maxdepth: 1
   :caption: Reference:

   executables
   API Reference <autoapi/bilby_pipe/index>
>>>>>>> docs/index.rst
